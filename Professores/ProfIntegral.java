public class ProfIntegral extends Professor
{
	private double salario;
	
	super(String nome, String matricula, int idade);
	
	public ProfIntegral(double salario)
	{
		this.salario = salario;
	}
	 
	public double getSalario()
	{
		return salario;
	}
		
	public void setSalario(double salario)
	{
		this.salario = salario;
	}
}
